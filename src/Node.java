
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   Node(){

   }

   public static Node parsePostfix (String s) {
      if (s.contains("()")){
         throw new RuntimeException("Tühjad  sulud");
      }else if(s.contains(",,")){
         throw new RuntimeException("topelt komad");
      }else if(s.contains(" ")){
         throw new RuntimeException("Tühik");
      }else if(s.length() == 0){
         throw new RuntimeException("Puu on tühi");
      }else if(s.contains("\t")){
         throw new RuntimeException("tabi viga");
      }else if(s.contains("((") & s.contains("))")){
         throw new RuntimeException("topelt sulud");
      } else if(s.contains(",") && !s.contains("(") && s.contains(")")) {
         throw new RuntimeException("String koosneb topelt juursõlmega" + s);
      }
      for(int i = 0; i < s.length(); i++){
         if(s.charAt(i) == '(' && s.charAt(i+1) == ',') {
            throw new RuntimeException("String koosneb koma errorist " + s);
         }
         if(s.charAt(i) == ')' && (s.charAt(i+1) == ',' || s.charAt(i+1) == ')')) {
            throw new RuntimeException("topelt paremsulg " + s);
         }
      }
      Stack<Node> stack = new Stack<>();
      Node newNode = new Node();
      List<String> pieces = new ArrayList<>();
      StringTokenizer st = new StringTokenizer(s, "(),", true);
      while(st.hasMoreTokens()) {
         pieces.add(st.nextToken().trim());
      }
      for(int i = 0; i < pieces.size(); i++){
         String token = pieces.get(i);
         if(token.equals("(")){
            stack.push(newNode);
            newNode.firstChild = new Node();
            newNode = newNode.firstChild;
         }else if( token.equals(")")){
            Node node = stack.pop();
            newNode = node;
         }else if(token.equals(",")){
            if(stack.empty())
               throw new RuntimeException("Comma exception" + s);
            newNode.nextSibling = new Node();
            newNode = newNode.nextSibling;
         }else{
            newNode.name = token;

            /**
             * @author: Egert Aia
             */
            if(i+1 < pieces.size()){
               String nextToken = pieces.get(i+1);
               if(nextToken.equals("(")){
                  throw new RuntimeException("Koma puudu! " + s);
               }


            }
            }
      }
      return newNode;
   }


   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.name);
      if (this.firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (this.nextSibling != null) {
         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "(B1(ffff)hh,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

